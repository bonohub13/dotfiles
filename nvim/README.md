# Dotfiles for neovim

# Where to place
- Copy this directory (nvim) under $HOME/.config

# Dependencies
- Colortheme
    - morhetz/gruvbox
        [URL](https://github.com/morhetz/gruvbox.git)

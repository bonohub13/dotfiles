# Dotfiles for my custom setup

# What dotfiles this repository includes
- zsh
- neovim
- tmux

## Not a dotfile but...
- installed\_arch\_pkgs
    - list of installed arch packages
